//@ts-check
import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'


ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <div className="bg-gradient-to-t from-custom-orange to-orange-400 min-h-screen">
            <App />
        </div>
    </React.StrictMode>
)
