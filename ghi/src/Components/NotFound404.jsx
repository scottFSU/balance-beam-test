import { Link } from "react-router-dom"

function PageNotFound() {
  
    return (
        <div className="flex flex-col items-center justify-center h-screen">
            <h1 className="mb-4">404 Page Not Found</h1>
            <button className="bg-black text-custom-orange flex items-center text 2xl mt-4 hover:bg-white font-bold py-2 px-4 rounded">
                <Link to="/">Go to homepage</Link>
            </button>
        </div>
    )
}

export default PageNotFound
