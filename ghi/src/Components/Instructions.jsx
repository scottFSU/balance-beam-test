import { Link } from "react-router-dom"

function Instructions() {

    return (
        <div className="lg:container lg:mx-auto">
            <div className="grid-auto-columns: min-content p-4 m-4">
                <div className="flex justify-start p-10 gap-x-4">
                    <h2>
                        <button className="button-black">
                            <Link to="/leaderboard">Leaderboard</Link>
                        </button>
                    </h2>
                    <h2>Instructions</h2>
                </div>
                <div className="w-full border-t border-gray-200"></div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/AdobeStock_255958510.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Equipment and Prep</h4>
                        <p className="instructions-p">
                            As with all workouts make sure you stretch and
                            hydrate before your workout. The Murph workout was
                            designed around you wearing a 20 pound weight vest
                            for men and 16 pound for women to simulate body
                            armor. We recommend you complete the workout at
                            least once without the weighted vest or backpack.
                        </p>
                    </div>
                </div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/AdobeStock_208459947.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Run 1</h4>
                        <p className="instructions-p">
                            Find a flat and safe run route that is .5 miles from
                            your start point. Run to your halfway point and turn
                            around back to your starting position.
                        </p>
                    </div>
                </div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/pullups.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Pull-ups</h4>
                        <p className="instructions-p">
                            Stand below the bar with your feet shoulder width
                            apart. Jump up and grip the bar with an overhand
                            grip about shoulder width apart. Fully extend your
                            arms so you are in a dead hang. One rep is pulling
                            yourself up so your chin is level with the bar.
                            Pause at the top. Lower yourself until your elbows
                            are straight. You will do 10 sets of 10 pull-ups.
                        </p>
                    </div>
                </div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/pushups.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Push-ups</h4>
                        <p className="instructions-p">
                            To do a push-up, lay face-down on the floor with
                            your palms on the floor shoulder-width apart and the
                            balls of your feet touching the ground. From there,
                            push yourself up into a full plank position, keeping
                            your body in a straight line. Lower yourself and
                            touch the floor with your chest to complete one rep.
                            For the Murph you will do 10 sets of 20 push-ups.
                        </p>
                    </div>
                </div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/squats.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Squats</h4>
                        <p className="instructions-p">
                            To do an air squat, stand with your heels just wider
                            than your hips and point your toes out slightly.
                            Push your hips back and down, descending to the
                            lowest point of your squat. The crease of your hips
                            should be below your knees. Maintain your balance in
                            your heels and drive through your heels to return to
                            the standing position. For the Murph you will do 10
                            sets of 30 air squats.
                        </p>
                    </div>
                </div>
                <div className="instructions-box">
                    <div className="image-div-instructions">
                        {
                            <img
                                src="/run2picture.jpeg"
                                alt="Description"
                                className="image-size-instructions"
                            />
                        }
                    </div>
                    <div className="instructions-div">
                        <h4 className="instructions-h4">Run 2</h4>
                        <p className="instructions-p">
                            Find a flat and safe run route that is .5 miles from
                            your start point. Run to your halfway point and turn
                            around back to your starting position.
                        </p>
                    </div>
                </div>
                <div className="flex justify-center items-center p-5 mb-4">
                    <button className="bg-black hover:bg-gray-700 text-white  py-2 px-4 rounded text-4xl">
                        <Link to="/start">Start Murph!</Link>
                    </button>
                </div>
                <div></div>
            </div>
        </div>
    )
}

export default Instructions
