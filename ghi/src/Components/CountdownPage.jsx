import { useState, useEffect } from 'react'
import {useNavigate, Link, Navigate} from 'react-router-dom'
import useUser from "../Common/useUser"


function Countdown() {
    const { user, isError, isLoading } = useUser()
    const [time, setTime] = useState(10);
    const navigate = useNavigate();

    useEffect(() => {
        const redirectTimer = setTimeout(() => {
        navigate('/workout');
        }, 11000);
        return () => clearTimeout(redirectTimer);
    }, [history])

    useEffect(() => {
        const timer = setInterval(() => {
        if (time > 1)
        setTime((prevTime) => prevTime - 1)
        }, 1000)
        return () => {
            clearInterval(timer);
        }
    }, []);

    if (isLoading) {
        return <div>...loading...</div>
    }
    if (isError) {
        return <div>There was an error</div>
    }
    if (!isLoading && !user) {
        return <Navigate to='/' />
    }

    return (
        <div className="flex flex-col items-center justify-center h-screen">
            <h3 className="mb-4">Your workout begins in...</h3>
            <h1 className="flex items-center justify-center text-9xl text-green-600 text-growth">{time > 0 ? time  : "GO!"}</h1>
            <button className="bg-green-600 text-black flex items-center text 2xl mt-4 hover:bg-white font-bold py-2 px-4 rounded">
                <Link to="/workout">Bypass and Begin!</Link>
            </button>
            <button className="bg-red-500 text-black flex items-center text 2xl mt-4 hover:bg-white font-bold py-2 px-4 rounded">
                <Link to="/dashboard">ABORT WORKOUT</Link>
            </button>
            <p className="mt-4">You will be redirected to your dashboard</p>
        </div>
    )
}

export default Countdown
