import { Card, Title, Subtitle, LineChart } from "@tremor/react"

function WorkoutLineChart({ lineChart }) {
    return (
        <Card>
            <Title className="text-orange-600">Your Progress</Title>
            <Subtitle></Subtitle>
            <LineChart
                className="mt-6"
                showLegend={false}
                noDataText="Finish a workout, Riley!"
                rotateLabelX={{
                    angle: 270,
                    verticalShift: 40,
                    xAxisHeight: 88,
                }}
                data={lineChart}
                index="name"
                categories={["Time"]}
                colors={["orange-600"]}
                yAxisWidth={40}
            />
        </Card>
    )
}

export default WorkoutLineChart
