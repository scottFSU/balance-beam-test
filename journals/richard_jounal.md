# Daily Journal

## 1/16/2024

-   Cloned BalanceBaem with Vite branch
-   Commented out fronted boilerplate and kept for reference
-   Docker is up and running with browser
-   Installed virtual environment
-   Installed requirements package
-   Must sign into pg-admin at localhost:8082
-   Username: root@root.com
-   PW: root

-   Created volumes
-   docker volume create balancebeam
-   docker volume create pg-admin
-   Balancebeam DB
-   Name balancebeam
-   Host name : db (this name is from docker-compose.yml)
-   Port : 5432
-   Maintenance database: postgres
-   Username: ramrod
-   Password: r@m_r0d

## 1/17/2024

-   Delete the old DBs
-   Added in env. To api directory
-   In docker.dev added migrations CMD
-   Linked docker-compose.yaml to .env
-   Passwords must only be A-Z 1-9

## 1/18/2024

-   Each table needs its own migration in dir called migrations 001_create_user_table.py
-   When making migrations you need to delete the DB for now
-   Research PGadmin and ways to do proper migrations
-   You can write tables in the migrations directory or in PGAdmin
-   I wrote the get_single_user router in the routers dir
-   We delt with a lot of merge issues that we worked out in the VSC merge editor

## 1/19/2024

-   We added the authenticator JWT to Balancebeam and worked on using the end points to add authentication
-   We added the JWTdown authenticator code from Curtis the “Narrow Programmer”, included AccountForm and AccountToken
-   Added the user routers: async def get_token, async def create_user, and def get_one_user
-   Added user queries for UsersQueries: def create and def get

## 1/20/2024

-   We got the authenticator to log in and return a hashed password
-   We got the authenticator to issue a token and allow a user to access their own information.
-   We added the hashed password to all current user routes

## 1/22/2024

-   Created the ability to added model via the workout queries and routers for POST
-   Created the queries for: def create_workout, def get_single_workout_by_id, def get_leaderboard, and def get_all_user_workouts
-   Added the queries for all workouts: def create_workout, def get_single_workout_by_id, def get_leaderboard, and def get_all_user_workouts
-   We added the authentication so that only a user can view their own workouts

## 1/23/2024

-   Removed user_id as it was not needed in the workout model
-   Use date time for all workout times using Zulu/GMT date time
-   When employing a router with "mine," they must be a child of the authorized logged-in route
-   We changed the time assessment for the workout model and used a date. Then, we calculated the time for sets and ran in the DB to get a duration renamed total_duration to duration.
-   We cleaned up the code with Flake8
-   Added the read_me template

## 1/24/2024

-   Removed get_single_user endpoint, preserving the query for use by `login`
-   Corrected the usage of user_id by the get_single_user query
-   Moved the `get_single_workout` to follow `get_user_workouts` so that it fixes the path collision issue

## 1/25/2024

-   I worked on the homepage and fixed some CSS to better space components.
-   I created a three-row structure on the nav bar to move all authorized user links to the right side.
-   I worked on importing an Excel file to PG Admin, but I am still looking for solutions.
-   Worked on some sitewide CSS
-   I'm still working on the readme.

## 1/26/2024

-   I started working on the Create Workout page and looked for a basic timer.
-   I found a way to display logged-in users' access to the dashboard(their user name), leaderboard, and a logout button.
-   I am working on the logout button to log a user out using the JWTdown module.

## 1/27/2024

-   Fixed some issues on the Navbar so that all links are flex-boxed and can size properly

## 1/29/2024

-   I worked on the createworkout page more.
-   We got the structure down to two primary columns, one with a table for sets and the other for counters and buttons.
-   We are working on displaying the workout set date in real-time with a button click.
-   I played with the Navbar to figure out the logout button. Use the same methodology.
-   I had issues figuring out how to do the counts, and Brendan helped me.

## 1/30/2024

-   I worked more on the create workout page, getting the counters and timer to work better.
-   Worked on the Navbar and moving image div so it could be larger.

## 1/31/2024
-	I worked on the instructions page and added all their relevant content.
-	I redesigned the instructions page to have a container with rows and columns.
-	I added pictures and text to all workout instructions
-	Added custom Tailwinds CSS for all instructions page elements

## 2/01/2024
-	Worked on the readme
-	Added journal entries 
-	Finalized instructions page
-	Worked on create workout page and got the time numbers to stop moving around other elements.





