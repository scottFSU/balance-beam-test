import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import './App.css';
import HomePage from './Components/HomePage'
import SignUpPage from './Components/SignUpPage';
import LoginPage from './Components/LoginPage';
import Leaderboard from './Components/Leaderboard';
import DashboardPage from './Components/DashboardPage';
import Instructions from './Components/Instructions';
import Nav from './Components/NavBar';
import Countdown from './Components/CountdownPage';
import CreateWorkout from './Components/CreateWorkout';
import PageNotFound from './Components/NotFound404';


const API_HOST = import.meta.env.VITE_API_HOST
const API_URL = import.meta.env.VITE_API_URL

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}


function App() {
    return (
        <BrowserRouter>
        <AuthProvider baseUrl={API_URL}>
            <Nav />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/signup" element={<SignUpPage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/leaderboard" element={<Leaderboard />} />
                <Route path="/dashboard" element={<DashboardPage />} />
                <Route path="/instructions" element={<Instructions />} />
                <Route path="/start" element={<Countdown/>} />
                <Route path="/workout" element={<CreateWorkout/>} />
                <Route path="*" element={<PageNotFound/>} />
            </Routes>
        </AuthProvider>
        </BrowserRouter>
    )
}

export default App
export { API_URL }
