import { API_URL } from "../App"
const fetchToken = async (navFn) => {
    const tokenURL = `${API_URL}/token`
    const fetchConfig = {
        credentials: "include",
    }
    const response = await fetch(tokenURL, fetchConfig)
    if (response.ok) {
        const data = await response.json()
        if (!data) {
            navFn("/login")
        }
    }
}

export default fetchToken;
