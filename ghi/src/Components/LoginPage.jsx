import useToken from "@galvanize-inc/jwtdown-for-react"
import useAuthContext from "@galvanize-inc/jwtdown-for-react"
import { useState, useEffect } from "react"
import { Link, useNavigate } from "react-router-dom"
import useUser from "../Common/useUser"

const LoginForm = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [submitted, setSubmitted] = useState(false)
    const { login } = useToken()
    const { token } = useAuthContext()

    const navigate = useNavigate()

    useEffect(() => {
        if (token) {
            navigate("/dashboard")
        }
    }, [token])

    const handleSubmit = (e) => {
        e.preventDefault()

        login(username, password)
        setTimeout(() => {
            setSubmitted(true);
        }, 400);
        
        setPassword("")
        }

    return (
        <div className="flex items-center justify-center p-4 ">
            <div className="w-full max-w-md p-8 text-black rounded">
                <form onSubmit={(e) => handleSubmit(e)} className="space-y-6">
                    <div className="flex inline items-center justify-center mb-3">
                        <label className="text-5xl mr-4" htmlFor="username">
                            Username:
                        </label>
                        <input
                            name="username"
                            type="text"
                            className="form-control"
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div className="flex inline items-center justify-center mb-3">
                        <label className="text-5xl mr-4" htmlFor="username">
                            Password:
                        </label>
                        <input
                            name="password"
                            type="password"
                            value={password}
                            className="form-control"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <button
                        type="submit"
                        className="w-full black bg-black text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    >
                        Login
                    </button>
                </form>
                <div className="text-center mt-6">
                     {submitted ? <div className="text-center text-red-800 mt-6">Incorrect Login Credentials. Please Try Again.</div> : ""}
                    Don"t have an account? <br />{" "}
                    <Link
                        className="text-orange-100 hover:text-orange-200"
                        to="/signup"
                    >
                        Signup Here
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default LoginForm
