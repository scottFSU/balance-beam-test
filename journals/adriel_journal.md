# Daily Journal
## 1/17/2024
- Today our team worked on setting up migrations/configuring out database. We
hit a major bug when adding psyco-pg and uncommenting out the migrations-up
command to the Dockerfile.dev. Turns out our password was too secure and could not
accept special characters. We spent over three hours on it, but we got it to work.
- Logged into pgadmin with new credentials

## 1/18/2024
- Today our team created our create_users table and our create_workouts tables and verified their creation in pg-admin.
- I had an extra "-" in my pg-admin volume, which caused my api container to break. This was strange to me because it was not breaking my volume for the past couple of days. Docker is weird! Even when it is working it should be taken with a grain of salt. Before that I updated Docker and had to restart my computer. After that, I was able to login to pgadmin and see our created tables, as well as checking in on swagger.
- We stubbed out all of our endpoints. The prerequesite to this was creating a new pydantic models folder and file. I pushed and committed the get_all_workouts API and worked with a team member on get_a_user.
- We purposely decided to create a merge conflict so we could handle a low-stakes merge conflict, and it still ended up taking quite a while to figure out. Real lesson here is to not pull and merge from main until it's up to date with all team members updates. Tomorrow we tackle auth!

## 1/19/2024
- We started auth and made quick progress configuring our Authenticator class by renaming its helper functions to meet our conventions.
- I drove while creating the post function for creating one user, which went pretty quickly, and pushed that.
- Scott drove while writing the get one function, which seemed to go fine, we got one user in swagger, but kept getting a 500 response. After about 2-3 hours of debugging we saw that we were returning the userIn model instead of the Authenticator token. Super simple mistake! Check examples and go line by line when debugging, not just writing print statements.

## 1/20/2024
- Richard drove and we wrote the get one token router. We had to change "accounts" to "users", but then after that we were able to get the token, added the logic to another router, and sure enough we could not hit our endpoints without being logged in, success!

## 1/25/2024
Team notes: Today we stubbed out some more front end components and worked on frontend authentication.
We created the nav page and updated the

## 1/26/2024
Team notes: Today was a little rough. We spent most of the day trying to get our SignUpPage to work. We initially tried using most of the logic from our login page, and after that we tried implementing the logic from the example repo. At the time of this entry we successfully signed up, but the token is still undefined.
Lesson learned from today is to hover over functions to see what their parameters are.

## Jan 29, 2024
Team notes: Pretty productive day today - we successfully started calling data from the backend and so things started moving fairly quickly. Richard added some styling and functionality to the navbar - a gradient and also hiding buttons from non-logged in users. Scott worked on the create-workout page and the hardest logic on the site - the counter. Brendan also worked on the counter and set up css for the page.
I copied over most of the css from the start workout page into the dashboard page and began the logic for the dynamic filling in of the username and the set data, aka duration and splits.

## Jan 30, 2024
Team notes: Today we fixed the timer on the workout page to increment exactly how we want it to; a total timer and a timer per set. We created redirect functionality for all of the pages, which was pretty hard - the pages would only redirect to the login page, and they would load temporarily but then error out. We also created links to navigate around the page, which was easy and fun. Scott created the timer for the countdown.
    - I tried working on the carousel but didn’t get very far.

## Jan 31, 2024
Team notes: Today we fixed all of the redirects around our site. There was some strange effects going on, such as after logging in the page would refresh, successfully redirecting you to another page, but not as a logged in user. It seems the site would successfully fetch the token, but the redirect would redirect you before the token was fetched. We solved this by implementing a wait time of one second on the redirecting of the page. We also created a modal for giving up too early on a page.
    - I created the finalized css for the login and signup pages, and helped cssify a couple other pages. I also finalized the carousel.

## Feb 1, 2024
Team notes: We had a slower day. Brendan and Scott worked on the redirect issue with the login and finally got it fixed. Richard and I stylized the create workout page.
    - I did some dumb things in git and lost some of my changes to the carousel, so i had to fix those along with dragging in new png images because the way I was importing them as urls was slowing our whole website down.

## Feb 5, 2024
Team notes: Today we handled more error handling. We worked on implementing errors for: incorrect password in the signup and login, unrecognized username, and incorrect password. Richard fixed the the missing username on the dashboard.
    - I drove adding a verified password on the backend. Just had to add an if statement and a raise and an error.

## Feb 6, 2024
Team notes: Today Richard and I paired on adjusting the tables on the dashboard and leaderboard pages, also small fixes to the homepage. Then we all transitioned over to working on our unit tests. Richard installed Tremor and got a draft graph up on the leaderboard page.
    - I worked with Richard on the above items, and finished my unit test.
