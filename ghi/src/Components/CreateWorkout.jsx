import { useState, useEffect } from "react"
import { useNavigate, Navigate } from "react-router-dom"
import { API_URL } from "../App"
import Modal from "./Modal";
import formatTime from "../Common/formatTime";
import formatIntervalTime from "../Common/formatIntervalTime";
import getTodaysDate from "../Common/getTodaysDate";
import useUser from "../Common/useUser"


function CreateWorkout() {
    const [time, setTime] = useState(0)
    const [intervalTime, setIntervalTime] = useState(0)
    const [lastClickTime, setLastClickTime] = useState(Date.now())
    const [splitTimes, setSplitTimes] = useState([])
    const [abandonInitiated, setAbandonInitiated] = useState(false)
    const [counter, setCounter] = useState({
        pull_ups: 0,
        push_ups: 0,
        squats: 0,
    })

    const { user, isError, isLoading} = useUser()
    const navigate = useNavigate()
    const workoutKeys = [
        "Run 1",
        "Set 1",
        "Set 2",
        "Set 3",
        "Set 4",
        "Set 5",
        "Set 6",
        "Set 7",
        "Set 8",
        "Set 9",
        "Set 10",
        "Run 2",
    ]
    const workoutTotals = {
        pull_ups: 100,
        push_ups: 200,
        squats: 300,
    }
    const setTotals = {
        pull_ups: 10,
        push_ups: 20,
        squats: 30,
    }
    const modalConfig = {
        title: `Abandon workout`,
        bodyText: `Are you sure? We believe in you! Summon your inner 12th man!\n\nIf you stop your workout early, you will be redirected to your dashboard.`,
        confirmButtonText: `Abandon Workout`,
        cancelButtonText: `Go Back`,
    }

    useEffect(() => {
        setIntervalTime(0)
        const intervalTimer = setInterval(() => {
        setIntervalTime((prevTime) => prevTime + 1)
        }, 1000)
        const sumSplitTimes = splitTimes.reduce((a,b) => a + b, 0)
        setTime(sumSplitTimes)
        return () => {
            clearInterval(intervalTimer)
        }
    }, [splitTimes])

    // Handlers
    // finish a set
    const handleFinishSet = () => {
        const now = Date.now()
        const timeSinceLastClick = now - lastClickTime

        // If less than 2 seconds has passed since the last click, ignore this click
        if (timeSinceLastClick < 2000) {
            return
        }
        // otherwise, reset the lastClickTime, set the splitTimes, and increment the counter
        setLastClickTime(now)
        setSplitTimes((prevSplitTimes) => [...prevSplitTimes, intervalTime])
        if (splitTimes.length > 0 && splitTimes.length < 11) {
            setCounter({
                pull_ups: counter.pull_ups + setTotals.pull_ups,
                push_ups: counter.push_ups + setTotals.push_ups,
                squats: counter.squats + setTotals.squats,
            })
        }
    }

    // finish a workout
    const handleFinishWorkout = async () => {
        const now = Date.now()
        const timeSinceLastClick = now - lastClickTime
        // If less than 2 seconds has passed since the last click, ignore this click
        if (timeSinceLastClick < 2000) {
            return
        }
        // Otherwise, set the split time, create a workout object, and submit the post request
        setSplitTimes((prevSplitTimes) => [...prevSplitTimes, intervalTime])

        const workout = {
            date: getTodaysDate(),
            run_1: splitTimes[0],
            set_1: splitTimes[1],
            set_2: splitTimes[2],
            set_3: splitTimes[3],
            set_4: splitTimes[4],
            set_5: splitTimes[5],
            set_6: splitTimes[6],
            set_7: splitTimes[7],
            set_8: splitTimes[8],
            set_9: splitTimes[9],
            set_10: splitTimes[10],
            run_2: intervalTime,
        }
        const workoutURL = `${API_URL}/api/workouts`
        const fetchConfigPost = {
            method: "post",
            body: JSON.stringify(workout),
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }

        const response = await fetch(workoutURL, fetchConfigPost)
        if (response.ok) {
            navigate("/dashboard")
        }
    }

    // bring up the abandon workout confirmation modal
    const handleModalDisplay = () => {
        setAbandonInitiated(!abandonInitiated)
    }

    // abandon a workout
    const handleConfirmAbandonWorkout = async () => {
        const workout = {
            date: getTodaysDate(),
            run_1: splitTimes[0],
            set_1: splitTimes[1],
            set_2: splitTimes[2],
            set_3: splitTimes[3],
            set_4: splitTimes[4],
            set_5: splitTimes[5],
            set_6: splitTimes[6],
            set_7: splitTimes[7],
            set_8: splitTimes[8],
            set_9: splitTimes[9],
            set_10: splitTimes[10],
            run_2: null,
        }
        const workoutURL = `${API_URL}/api/workouts`
        const fetchConfigPost = {
            method: "post",
            body: JSON.stringify(workout),
            headers: {
                "Content-Type": "application/json",
            },
            credentials: "include",
        }

        const response = await fetch(workoutURL, fetchConfigPost)
        if (response.ok) {
            navigate("/dashboard");
        };
    };
        if (isLoading) {
            return <div>...loading...</div>
        }
        if (isError) {
            return <div>There was an error</div>
        }
        if (!isLoading && !user) {
            return <Navigate to="/" />
        }
    return (
        <>
            {abandonInitiated ? (
                <Modal
                    textConfig={modalConfig}
                    confirmFn={handleConfirmAbandonWorkout}
                    cancelFn={handleModalDisplay}
                />
            ) : (
                ""
            )}
            
            <div className="standard-page-container">
                <div className="flex row items-stretch text-center items-start justify-center mx-auto">
                    <div className="row bg-white shadow-lg rounded-lg p-5 mt-4 text-center">
                        <h1>Let"s do a Murph!</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="flex row items-stretch text-center items-start justify-center mx-auto">
                        <div className="grid m-2">
                            <div className="row bg-white shadow-lg rounded-lg p-5 my-4 ">
                                <h1> Workout Splits</h1>
                                <table className="w-full table-auto mx-auto border border-black">
                                    <thead>
                                        <tr>
                                            <th className="p-1">Split</th>
                                            <th className="p-1">Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {workoutKeys.map((value, index) => (
                                            <tr
                                                key={index}
                                                className={
                                                    index % 2 === 0
                                                        ? "bg-gray-200 border border-black"
                                                        : "border border-black"
                                                }
                                            >
                                                <td>{value}</td>
                                                <td className="font-mono">
                                                    {splitTimes[index]
                                                        ? formatIntervalTime(
                                                              splitTimes[index]
                                                          )
                                                        : index === 0 ||
                                                          splitTimes[index - 1]
                                                        ? formatIntervalTime(
                                                              intervalTime
                                                          )
                                                        : "--"}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="flex items-start justify-start m-2">
                            <div className="flex flex-col space-y-4 bg-white shadow-lg rounded-lg p-5 my-4">
                                <h1>Workout Totals/Remaining</h1>
                                {/* row 1 is workout timer, workout totals/remaining */}
                                <div className="grid grid-cols-4 gap-4 items-center text-center">
                                    <p className="col-span-1">Reps per set:</p>
                                    <p>Pull-ups: 10</p>
                                    <p>Push-ups: 20</p>
                                    <p>Squats: 30</p>
                                </div>
                                <div className="grid grid-cols-4 gap-4 mx-auto">
                                    <div className="create-work-out-page-textbox">
                                        <p className="">Timer</p>
                                        <p className="mx-auto font-mono">
                                            {formatTime(time + intervalTime)}
                                        </p>
                                    </div>
                                    <div className="create-work-out-page-textbox">
                                        <p>Pull-ups</p>
                                        <p>
                                            {counter.pull_ups} / {}
                                            {workoutTotals.pull_ups -
                                                counter.pull_ups}
                                        </p>
                                    </div>
                                    <div className="create-work-out-page-textbox">
                                        <p>Push-ups</p>
                                        <p>
                                            {counter.push_ups} / {}
                                            {workoutTotals.push_ups -
                                                counter.push_ups}
                                        </p>
                                    </div>
                                    <div className="create-work-out-page-textbox">
                                        <p>Squats</p>
                                        <p>
                                            {counter.squats} / {}
                                            {workoutTotals.squats -
                                                counter.squats}
                                        </p>
                                    </div>
                                </div>
                                {/* this row if for the finish run/set button and give up button */}
                                <div className="flex justify-center items-center mt-4">
                                    {splitTimes.length >= 11 ? (
                                        <button
                                            onClick={handleFinishWorkout}
                                            className="text-center bg-black hover:bg-gray-700 text-green-500 font-bold py-2 px-4 rounded m-4"
                                        >
                                            Finish Workout
                                        </button>
                                    ) : (
                                        <button
                                            onClick={handleFinishSet}
                                            className="text-center bg-black hover:bg-gray-700 text-green-500 font-bold py-2 px-4 rounded m-4"
                                        >
                                            Finish Set
                                        </button>
                                    )}
                                    <button
                                        onClick={handleModalDisplay}
                                        className="text-center bg-black hover:bg-gray-700 text-red-500 font-bold py-2 px-4 rounded m-4"
                                    >
                                        End Workout Early
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CreateWorkout
