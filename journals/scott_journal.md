# Daily Journal

## 1/17/2024
Team notes:
    - Knocked out several quick linear tickets including env file creation and env variable creation.
    - Became stuck with a bug during psycogp installation and connecting to the database. We worked through the problem for several hours befor realizing the password complexity was throwing the code off due to special characters creating conflict.
    - We didn’t get as far as we wanted to, but seem to be in a good spot. We should be able to get our tables complete tomorrow, stub out our endpoints, and get a solid start on authorization.
Personal notes:
    - Committed two changes (env creation and env variable creation).

## 1/18/2024
Team notes:
    - Knocked out api endpoints.
    - Validated fixes to Wednesday night bugs.
Personal notes:

