import { useState, useEffect } from "react"
import { NavLink, useNavigate } from "react-router-dom"
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react"
import { API_URL } from "../App"
import useToken from "@galvanize-inc/jwtdown-for-react"
import fetchData from "../Common/fetchData"

function Nav() {
    const { token } = useAuthContext()
    const { logout } = useToken()
    const [userInfo, setUserInfo] = useState(null)
    const navigate = useNavigate()

    const tokenURL = `${API_URL}/token`
    const fetchConfig = {
        credentials: "include",
    }

    useEffect(() => {
        fetchData(tokenURL, fetchConfig, setUserInfo)
    }, [tokenURL, fetchConfig, setUserInfo])

    const logUserOut = (e) => {
        e.preventDefault()
        logout()
        setUserInfo(null)
        navigate(`/`)
    }

    return (
        <nav className="flex w-full bg-custom-black items-center justify-between p-4">
            <NavLink to="/">
                <img
                    src="/balancebeam.png"
                    alt="Description"
                    className="h-20 w-20"
                />
            </NavLink>
            <div className="mr-auto flex items-start">
                <button className="button-orange">
                    <NavLink to="/instructions">The Murph</NavLink>
                </button>
                <button className="button-orange">
                    <NavLink to="/start">Start a Murph</NavLink>
                </button>
            </div>
            <div className="ml-auto flex items-end">
                {token ? (
                    <>
                        <button className="button-orange">
                            <NavLink to="/dashboard">
                                Hi {userInfo && userInfo.user.username}!
                            </NavLink>
                        </button>
                        <button className="button-orange" onClick={logUserOut}>
                            Logout
                        </button>
                    </>
                ) : (
                    <button>
                        <NavLink className="button-orange" to="/login">
                            Login
                        </NavLink>
                    </button>
                )}
            </div>
        </nav>
    )
}

export default Nav
