// this takes milliseconds and return HH:MM:SS
const formatIntervalTime = (time) => {
    const hours = Math.floor(time / 3600)
    const minutes = Math.floor((time % 3600) / 60)
    const seconds = parseInt(time % 60)
    return `${minutes
        .toString()
        .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`
}

export default formatIntervalTime;
