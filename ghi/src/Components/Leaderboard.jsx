import { useEffect, useState } from "react"
import { Link, Navigate } from "react-router-dom"
import fetchData from "../Common/fetchData"
import { API_URL } from "../App"
import useUser from "../Common/useUser"
import formatUserTime from "../Common/formatUserTime"

function ListLeaderboard() {
    const { user, isError, isLoading } = useUser()
    const [leaderboard, setLeaderboard] = useState([])

    const leaderboarURL = `${API_URL}/api/workouts/leaderboard`
    const fetchConfig = {
        credentials: "include",
    }

    useEffect(() => {
        // fetchToken(navigate);
        fetchData(leaderboarURL, fetchConfig, setLeaderboard)
    }, [])
    if (isLoading) {
        return <div>...loading...</div>
    }
    if (isError) {
        return <div>There was an error</div>
    }
    if (!isLoading && !user) {
        return <Navigate to="/" />
    }

    return (
        <div className="container mx-auto p-4">
            <div className="flex p-10 gap-x-4">
                <h2>Leaderboard</h2>
                <h2>
                    <button className="button-black">
                        <Link to="/instructions">Instructions</Link>
                    </button>
                </h2>
            </div>
            <div className="w-full border-t border-gray-200"></div>
            <table className="ml-10 mt-10 w-auto border border-black justify-start text-center">
                <thead>
                    <tr>
                        <th className="table-margin-padding">Rank</th>
                        <th className="table-margin-padding">Leader</th>
                        <th className="table-margin-padding">Time</th>
                        <th className="table-margin-padding">Date</th>
                    </tr>
                </thead>
                <tbody>
                    {leaderboard.map((leader, index) => {
                        return (
                            <tr
                                key={leader.id}
                                className={
                                    index % 2 === 0
                                        ? "bg-gray-200 border border-black"
                                        : "border border-black"
                                }
                            >
                                <td className="table-margin-padding">
                                    {index + 1}
                                </td>
                                <td className="table-margin-padding">
                                    {leader.username}
                                </td>
                                <td className="table-margin-padding">
                                    {formatUserTime(leader.duration)}
                                </td>
                                <td className="table-margin-padding">
                                    {leader.date}
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ListLeaderboard
