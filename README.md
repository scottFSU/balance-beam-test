# Balancebeam

​
Team:
​

-   Adriel Stokes
-   Brendan Forster
-   Richard A Hofmann-Warner
-   Scott Cunningham
    ​

## Design

​
**Instructions on how to run application:**
​

```
​
1. Go to remote repository using this link: https://gitlab.com/hr-gamma-ramrod/balance-beam
2. Click on blue 'Code' dropdown button
3. Click on clipboard icon that copies the url corresponding to 'Clone with HTTPS'
4. Open up a terminal window and run the command: cd into whatever directory you would like to clone this project into
5. Once in that directory, run the command: git clone https://gitlab.com/hr-gamma-ramrod/balance-beam
6. Run the command: ls (to see what the name of the cloend project is)'
7. Run the command: cd in to that directory
8. Run the command: docker-compose up to get all of the necessary docker containers up and running
9. Type localhost:5173 into your browser to access the application
10. Explore!
​
```

​
**Diagram of Bounded Contexts:**

-   Balance Beam Design
    ![URL](https://www.figma.com/file/6xMLMelT1IbLp4aVizRlTf/BalanceBeam?type=design&node-id=0-1&mode=design&t=XVve44n683h0KT1A-0)

​

```
​
**High level design:**
​
```

​
Balancebeam is made up of 1 React application and 1 prostgrSQL database:
​ -_Vite React application_ 5173

-   _prostgrSQL_ 15324:5432
-   _PG-Admin_ 8082:80
-   _fastapi_ 8000:8000
    ​

```

​
**Models Description:**
​
```

We created the models and placed them in models directory . Inside the models directory we had: DuplicateUserError, Error, UserIn, UserOut, UserOutWithHashedPassword,
WorkoutIn, WorkoutOut, LeaderboardOut, AccountForm, AccountToken, and HttpError.
​
The we have three standard models for handling backend functions DuplicateUserError, Error, and HttpError:

DuplicateUserError to ensure that only one user has that same properties:

-   class DuplicateUserError(ValueError):
    _pass_

The Error returns errors when applicable

-   class Error(BaseModel):
    _message: str_

HttpError is for errors with responses

-   class HttpError(BaseModel):
    _detail: str_

The User model dictates how a user is created in the database:

-           _username_
-           _password_

Balancebeam has three models to support the user model:

UserIn that dictates how a user's information is put into the models.

-   class UserIn(BaseModel):
    _username: str_
    _password: st_

UserOut that dictates how a user's information sent out.

-   class UserOut(BaseModel):
    _id: int_
    _username: str_

UserOutWithHashedPassword returns a hashed password when a user logs in.

-   class UserOutWithHashedPassword(UserOut):
    _hashed_password: str_

```
​
**API Endpoints**
​
```

| Action       | Method | URL                              |
| ------------ | ------ | -------------------------------- |
| Create Users | Post   | http://localhost:8000/api/users/ |
| Create Users | Get    | http://localhost:8000/api/token  |

---

## | Create a User | POST | http://localhost:8000/api/users/

## | Get user token | GET | http://localhost:8000/api/token

```
​
*To create a new user, send a JSON body following this format:_

```

{
"username": "string",
"password": "string"
}
​

```

```

| Action       | Method | URL                             |
| ------------ | ------ | ------------------------------- |
| Create Users | Post   | http://localhost:8000/api/token |
| Create Users | Delete | http://localhost:8000/api/token |

---

## | Create a User | POST | http://localhost:8000/api/token

## | Get user token | DELETE | http://localhost:8000/api/token

```
​
*To create a user's token, send a JSON body following this format:_

```

{
"access_token": "string",
"token_type": "Bearer"
}
​

```
​
*To create a user's token, send a JSON body following this format:_

```

{
"access_token": "string",
"token_type": "Bearer"
}
​

```

```

​
| Action | Method | URL |
| -------------- | -------- | ------------------------------------ |
| Create Workout | Post | http://localhost:8000/api/workouts/ |
| Create Workout | Get | http://localhost:8000/api/workouts/ |

---

## | Create a Workout | POST | http://localhost:8000/api/workouts

## | Create a Workout | POST | http://localhost:8000/api/workouts

## | Get Single workout by Id | GET | http://localhost:8000/api/workouts/mine/{id}

## | Get Leaderboard | GET | http://localhost:8000/api/workouts/leaderboard

## | Get all user workouts | GET | http://localhost:8000/api/workouts/mine

```

​
\*To create a new user, send a JSON body following this format:\_

```

{
"date": "2024-02-01",
"run_1": 0,
"set_1": 0,
"set_2": 0,
"set_3": 0,
"set_4": 0,
"set_5": 0,
"set_6": 0,
"set_7": 0,
"set_8": 0,
"set_9": 0,
"set_10": 0,
"run_2": 0
}
​

```

_To get single workout by Id, send a JSON body following this format:_

```

{
"date": "2024-02-01",
"run_1": 0,
"set_1": 0,
"set_2": 0,
"set_3": 0,
"set_4": 0,
"set_5": 0,
"set_6": 0,
"set_7": 0,
"set_8": 0,
"set_9": 0,
"set_10": 0,
"run_2": 0,
"user_id": 0,
"is_completed": true,
"id": 0
}
​

```

_To get the leaderboard, send a JSON body following this format:_

```

[
{
"date": "2024-02-01",
"run_1": 0,
"set_1": 0,
"set_2": 0,
"set_3": 0,
"set_4": 0,
"set_5": 0,
"set_6": 0,
"set_7": 0,
"set_8": 0,
"set_9": 0,
"set_10": 0,
"run_2": 0,
"user_id": 0,
"is_completed": true,
"id": 0,
"duration": 0,
"username": "string"
}
]
​

```

​*To get all the of a single user's workouts, send a JSON body following this format:*

```

{
"message": "string"
}
​

```

```
