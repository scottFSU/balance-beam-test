import Carousel from "./carousel"
import { Link } from "react-router-dom"
import amanda from "../../public/amanda.png"
import brad from "../../public/brad.png"
import britney from "../../public/britney.png"
import mike from "../../public/mike.png"

const slides = {
    1: amanda,
    2: brad,
    3: britney,
    4: mike,
}

function HomePage() {
    return (
        <div className="container mx-auto p-4 flex ">
            <div className="justify-center items-center">
                <div className="white-text-box">
                    <h2>Balance Beam</h2>
                    <p>
                        BalanceBeam is an application that enables users to
                        track their fitness performance for the Murph workout. It
                        includes the ability to log a workout,
                        track your performance over time, and see the results of
                        other users.
                    </p>
                </div>
                <div className="standard-div flex">
                    <button className="button-black mt-20 mb-4 transition duration-500 hover:scale-150">
                        <Link to="/instructions">Get Swole</Link>
                    </button>
                </div>
                <div className="standard-div mt-20 mb-8">
                    <img
                        src="/AdobeStock_255958510.jpeg"
                        alt="Description"
                        className="standard-image max-w-2xl mx-auto "
                    />
                </div>
                <div className="standard-div flex">
                    <div
                        className="max-w-lrg overflow-hidden relative mt-20 mb-8"
                        style={{ maxWidth: "800px" }}
                    >
                        <Carousel>
                            {Object.entries(slides).map((slide) => (
                                <img
                                    src={slide[1]}
                                    key={slide[0]}
                                    className="standard-image mt-8"
                                />
                            ))}
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage
