const fetchData = async (requestURL, fetchConfig={}, stateUpdater) => {
    const response = await fetch(requestURL, fetchConfig)
    if (response.ok) {
        const data = await response.json()
        stateUpdater(data)
    }
}

export default fetchData
